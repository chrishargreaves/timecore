import unittest
import lib_timecore.unix_time

class MyTestCase(unittest.TestCase):

    def test_passing_bytes_gets_results(self):
        self.assertEqual(0, lib_timecore.unix_time.decode(b'\x00\x00\x00\x00'))

    def test_passing_wrong_no_bytes_raises(self):
        self.assertRaises(TypeError, lib_timecore.unix_time.decode, b'\x00')
        self.assertRaises(TypeError, lib_timecore.unix_time.decode, b'\x00\x00\x00\x00\x00')



if __name__ == '__main__':
    unittest.main()
