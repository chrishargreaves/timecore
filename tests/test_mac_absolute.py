import unittest
import lib_timecore.mac_absolute

class MyTestCase(unittest.TestCase):
    def test_something(self):
        res = lib_timecore.mac_absolute.decode(482595381.713675)
        self.assertEqual(1460902581.713675, res)


if __name__ == '__main__':
    unittest.main()
