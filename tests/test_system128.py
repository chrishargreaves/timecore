import unittest
import lib_timecore.system128

class MyTestCase(unittest.TestCase):

    def test_128_bit_system_time_method(self):
        # DB 07 0B 00 06 00 13 00 12 00 11 00 17 00 3A 03
        # Sat, 19 November 2011 18:17:23.826 (from DCode)
        res = lib_timecore.system128.decode(b"\xdb\x07\x0b\x00\x06\x00\x13\x00\x12\x00\x11\x00\x17\x00\x3a\x03")
        self.assertEquals(res, 1321726643.000826)
        # DF 07 0C 00 04 00 18 00 0F 00 37 00 0D 00 16 01
        # Thu, 24 December 2015 15:55:13.278 (from DCode)
        res = lib_timecore.system128.decode(b"\xDF\x07\x0C\x00\x04\x00\x18\x00\x0F\x00\x37\x00\x0D\x00\x16\x01")
        self.assertEquals(res, 1450972513.000278)


    def test_128_system_time_not_bytes_raises(self):
        self.assertRaises(TypeError, lib_timecore.system128.decode, 1)
        self.assertRaises(TypeError, lib_timecore.system128.decode, 1.0)
        self.assertRaises(TypeError, lib_timecore.system128.decode, 'a string')

if __name__ == '__main__':
    unittest.main()

