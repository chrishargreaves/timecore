import unittest
import lib_timecore.chrome_time

class MyTestCase(unittest.TestCase):

    def test_decode(self):
        res = lib_timecore.chrome_time.decode(13105641603000000)
        self.assertEqual(1461168003, res)


    def test_encode(self):
        res = lib_timecore.chrome_time.encode_to_number('2016-04-20T16:03:00')
        self.assertEqual(13105641603000000, res)


if __name__ == '__main__':
    unittest.main()
