import unittest
import lib_timecore.file_time


class TestFileTime(unittest.TestCase):

    def test_filetime_decodes(self):
        # 24/12/2015 16:15:05
        self.assertIsInstance(lib_timecore.file_time.decode(b'\x13\xD9\x5F\x40\x66\x3E\xD1\x01'), float)
        self.assertAlmostEqual(1450973705.8064659, lib_timecore.file_time.decode(b'\x13\xD9\x5F\x40\x66\x3E\xD1\x01'))

    def test_filetime_raises_type_error(self):
        self.assertRaises(TypeError, lib_timecore.file_time.decode, -1)

    def test_filetime_raises_value_error(self):
        self.assertRaises(ValueError, lib_timecore.file_time.decode, 0)
        self.assertRaises(ValueError, lib_timecore.file_time.decode, b'\x00\x00\x00\x00\x00\x00\x00\x00')

    def test_bad_list_raises(self):
        self.assertRaises(ValueError, lib_timecore.file_time.decode, [])
        self.assertRaises(ValueError, lib_timecore.file_time.decode, [1])
        self.assertRaises(ValueError, lib_timecore.file_time.decode, [1,2,3])
        self.assertRaises(ValueError, lib_timecore.file_time.decode, ['a', 'a'])
        self.assertRaises(ValueError, lib_timecore.file_time.decode, [1, 'a'])

    def test_list_gets_time(self):
        self.assertEqual(1449187338.1199286, lib_timecore.file_time.decode([0x0A42ADB6,
                                                                               0x01D12E27]))
        self.assertEqual(1448646357.7585907, lib_timecore.file_time.decode([0x78D678F4,
                                                                               0x01D1293B]))
        self.assertEqual(1448645029.863031, lib_timecore.file_time.decode([0x6159B0A6,
                                                                               0x01D12938]))
        self.assertEqual(1448620649.0736856, lib_timecore.file_time.decode([0x9D4478D9,
                                                                               0x01D128FF]))
        self.assertEqual(1448639263.53785, lib_timecore.file_time.decode([0xF45A4B44,
                                                                               0x01D1292A]))


if __name__ == '__main__':
    unittest.main()
