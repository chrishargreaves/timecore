import unittest
import timecore

class MyTestCase(unittest.TestCase):

    def test_filetime(self):
        self.assertEqual(1450973705.8064659, timecore.decode(b'\x13\xD9\x5F\x40\x66\x3E\xD1\x01',
                                                             'filetime'))
        self.assertEqual('2015-12-24T16:15:05.806466', timecore.decode(b'\x13\xD9\x5F\x40\x66\x3E\xD1\x01',
                                                             'filetime', 'iso'))
        self.assertEqual('2015-12-24 16:15:05.806466', timecore.decode(b'\x13\xD9\x5F\x40\x66\x3E\xD1\x01',
                                                             'filetime', 'iso2'))



    def test_unixtime(self):
        # todo
        pass

    def test_system128(self):
        #todo
        pass

if __name__ == '__main__':
    unittest.main()
