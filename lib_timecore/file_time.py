import struct


def decode(the_value):
    """Decodes a Windows FILETIME 64-bit value
    will also take a list of 2 ints, low high"""
    if isinstance(the_value, bytes):
        int_val = struct.unpack("<Q", the_value)
        return decode_from_int(int_val[0])
    elif isinstance(the_value, int):
        return decode_from_int(the_value)
    elif isinstance(the_value, list):
        if len(the_value) != 2:
            raise ValueError('Supplying a list requires 2 integers')
        if type(the_value[0]) != int or type(the_value[1]) != int:
            raise ValueError('Supplying a list requires 2 integers')
        return decode_from_two_ints(the_value)
    else:
        raise TypeError('decode() requires bytes or int')


def decode_from_two_ints(list_of_ints):
    packed_bytes = struct.pack('<II', list_of_ints[0], list_of_ints[1])
    big_int = struct.unpack("<Q", packed_bytes)[0]
    return decode_from_int(big_int)


def decode_from_int(the_value):
    """Returns a Unix time given an int version of a Windows FILETIME"""
    if the_value <= -1:
        raise TypeError("Cannot convert negative WindowsTime")
    if the_value == 0:
        raise ValueError('Zero time supplied')
    else:
        timestamp_data = the_value - 116444736000000000
        unix_timestamp = timestamp_data/10000000
        return unix_timestamp


def encode(the_value):
    pass


