import struct
import datetime
import time

def decode(the_bytes):
    """Decodes a 128 bit SYSTEM time to unix time"""
    # https://msdn.microsoft.com/en-us/library/ms724950(VS.85).aspx
    if isinstance(the_bytes, bytes):
        the_year, the_month, the_day_of_seek, the_day, the_hour, \
            the_min, the_sec, the_thousanth = struct.unpack("<HHHHHHHH", the_bytes)

        #if the_year == 0:
        # this can't decode hte version of this where the year is zero because it has to return a
        # unix time, which can't handle that

        python_datetime = datetime.datetime(the_year, the_month, the_day, the_hour, the_min, the_sec, the_thousanth)
        unix_time = time.mktime(python_datetime.timetuple()) + 1e-6 * python_datetime.microsecond
        return unix_time
    else:
        raise TypeError('decode requires bytes')

def encode(the_time):
    pass
