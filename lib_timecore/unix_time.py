import datetime
import time
import struct

def decode(the_value):
    if isinstance(the_value, int) or isinstance(the_value, float):
        python_datetime = __decode_from_number(the_value)
        unix_time = time.mktime(python_datetime.timetuple()) + 1e-6 * python_datetime.microsecond
        return unix_time
    elif isinstance(the_value, bytes):
        if len(the_value) != 4:
            raise TypeError('Unix time requires 4 bytes (32 bits)')
        result = struct.unpack('<I', the_value)
        return result[0]
    elif isinstance(the_value, str):
        raise TypeError('str not supported for decoding. Try passing bytes')
    else:
        raise TypeError('Unsupported type passed to decode ({})'.format(type(the_value)))



def __decode_from_number(the_int):
    result = datetime.datetime.utcfromtimestamp(the_int)
    return result


def __decode_from_hex(the_hex):
    pass


def encode(the_value):
    pass


def __encode_to_number(iso_string):
    pass


def __encode_to_hex(iso_string):
    pass


def __encode_to_hex_no_spaces(iso_string):
    pass

