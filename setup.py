from distutils.core import setup
setup(name='timecore',
      version='0.1',
      description='Decoding dates and times in Python',
      author='Chris Hargreaves',
      author_email='chris@hargs.co.uk',
      py_modules=['timecore',
                  'lib_timecore.file_time',
                  'lib_timecore.system128',
                  'lib_timecore.mac_absolute',
                  'lib_timecore.unix_time'],
      )