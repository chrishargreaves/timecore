#! /usr/local/bin/python3

import logging
import argparse
import sys
import datetime

import lib_timecore.unix_time
import lib_timecore.file_time
import lib_timecore.system128
import lib_timecore.mac_absolute




available_formats = {'unixtime': {'description': 'Unix time',
                                  'decode': lib_timecore.unix_time.decode,
                                  'encode': lib_timecore.unix_time.encode},
                     'system128': {'description': '128 bit SYSTEM Time',
                                  'decode': lib_timecore.system128.decode,
                                  'encode': lib_timecore.system128.encode},
                     'filetime': {'description': 'Windows FILETIME',
                                  'decode': lib_timecore.file_time.decode,
                                  'encode': lib_timecore.file_time.encode},
                     'macabs': {'description': 'Mac Absolute Time',
                                  'decode': lib_timecore.mac_absolute.decode,
                                  'encode': lib_timecore.mac_absolute.encode}

                     }

def print_available_formats():
    out_str = ""
    for each in sorted(available_formats):
        out_str += "{}\t{}\n".format(each, available_formats[each]['description'])
    return out_str


def decode(value, time_format, out_format='unixtime'):
    """"""
    if time_format in available_formats:
        res = available_formats[time_format]['decode'](value)
        if out_format == 'unixtime':
            return res
        elif out_format == 'iso':
            result = datetime.datetime.utcfromtimestamp(res)
            return result.isoformat()
        elif out_format == 'iso2':
            result = datetime.datetime.utcfromtimestamp(res)
            return result.isoformat(' ')
    else:
        raise TypeError('Invalid time format specified')


def encode(value, the_format):
    """"""
    if the_format in available_formats:
        pass
    else:
        raise TypeError('Invalid format')




if __name__ == "__main__":

    logging.basicConfig(format='%(asctime)s %(levelname)-8s %(message)s',
                        level=logging.ERROR)
    logging.info("timecore launched")

    # Parse all the arguments passed to the program
    parser = argparse.ArgumentParser(description='Encode and decode dates and times')
    parser.add_argument('-f', '--format', type=str,
                        help='date/time format to use. Use --format list to show those available')
    parser.add_argument('-d', '--decode', action="store_true",
                        help='Decode a date/time')
    parser.add_argument('-e', '--encode', action="store_true",
                        help='Encode a date/time')
    parser.add_argument('-i', '--input', type=str,
                        help='Set format for input (number(default), hex, iso_string)')
    parser.add_argument('-o', '--output', type=str,
                        help='Set format for output (number, hex, hex_no_spaces, iso_string)')
    parser.add_argument('-tz', '--timezone', type=int,
                        help='Apply timezone to output')
    parser.add_argument('value', type=str,
                        help='the value to decode/encode')

    args = parser.parse_args()

    if not args.format:
        parser.print_help()
        sys.exit(-1)

    if args.format == 'list':
        print(lib_timecore.print_available_formats())
        sys.exit(0)


    if args.decode and not args.encode:
        # we are decoding
        result = lib_timecore.decode(float(args.value), args.format)
        print(result)
    elif args.encode and not args.decode:
        # we are encoding
        pass
    else:
        parser.print_usage()
        sys.exit(-1)



    sys.exit(0)